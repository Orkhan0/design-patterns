package design_patterns.creational.builder;

public class BuilderPattern {

    public static void main(String[] args) {
        System.out.println("start");
        Computer computer = new Computer.ComputerBuilder("500GB", "2GB")
                .setBluetoothEnabled(true)
                .setGraphicsCardEnabled(true)
                .setSSD("ASUS SSD")
                .build();
        System.out.println(computer);
    }

}