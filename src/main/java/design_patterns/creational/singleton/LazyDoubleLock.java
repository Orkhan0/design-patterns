package design_patterns.creational.singleton;

public class LazyDoubleLock {

    private static LazyDoubleLock instance;
    public String str;

    private LazyDoubleLock() {
        str = "This is fully Singleton";
    }

    public static LazyDoubleLock getInstance() {
        if (instance == null) {
            synchronized (LazyDoubleLock.class) {
                if (instance == null) {
                    instance = new LazyDoubleLock();
                }
            }
        }
        return instance;
    }

}