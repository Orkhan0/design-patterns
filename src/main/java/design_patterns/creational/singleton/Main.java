package design_patterns.creational.singleton;

import java.util.Locale;

public class Main {

    public static void main(String[] args) {
        LazyDoubleLock text = LazyDoubleLock.getInstance();
        System.out.println(text.str);

        text.str = (text.str).toUpperCase();
        System.out.println(text.str);
    }

}