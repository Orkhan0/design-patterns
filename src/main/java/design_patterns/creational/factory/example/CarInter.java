package design_patterns.creational.factory.example;

public interface CarInter {

    String getColor();

    double getPrice();

    int modelYear();

    void showCarInfo();

    boolean backupCamera();

}