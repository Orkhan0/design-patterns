package design_patterns.creational.factory.example;

public class Audi implements CarInter {

    private String color;
    private double price;
    private int modelYear;
    private boolean backupCamera;

    public Audi(String color, double price, int modelYear, boolean backupCamera) {
        this.color = color;
        this.price = price;
        this.modelYear = modelYear;
        this.backupCamera = backupCamera;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public int modelYear() {
        return modelYear;
    }


    @Override
    public boolean backupCamera() {
        return backupCamera;
    }

    @Override
    public void showCarInfo() {
        System.out.println("Car mark: Audi");
        System.out.println("Car price: " + price);
        System.out.println("Car model year: " + modelYear);
        if (backupCamera) {
            System.out.println("Backup Camera: Exist");
        } else {
            System.out.println("Backup Camera: Not Exist");
        }
    }

}