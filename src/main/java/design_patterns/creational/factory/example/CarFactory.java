package design_patterns.creational.factory.example;

public class CarFactory {

    public static CarInter createCar(String mark, String color, double price, int modelYear, boolean backupCamera) {
        CarInter car;
        if (mark.equals("BMW")) {
            car = new BMW(color, price, modelYear, backupCamera);
        } else if (mark.equals("Audi")) {
            car = new Audi(color, price, modelYear, backupCamera);
        } else {
            throw new RuntimeException(mark + " This mark doesn't exist");
        }
        return car;
    }

}