package design_patterns.creational.factory.example;

public class FactoryPatternExample {

    public static void main(String[] args) {
        CarInter bmw = CarFactory.createCar("BMW", "Black", 330000, 2020, true);
        CarInter audi = CarFactory.createCar("Audi", "Green", 210000, 2019, false);

        bmw.showCarInfo();
        System.out.println("--- --- --- --- ---");
        audi.showCarInfo();
    }

}