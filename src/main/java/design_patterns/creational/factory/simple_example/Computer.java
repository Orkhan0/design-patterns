package design_patterns.creational.factory.simple_example;

public interface Computer {

    void name();

    void since(int year);

}