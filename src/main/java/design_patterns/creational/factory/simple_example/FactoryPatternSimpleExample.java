package design_patterns.creational.factory.simple_example;

public class FactoryPatternSimpleExample {

    public static void main(String[] args) {
        try {
            Asus asus = (Asus) ComputerFactory.createComputer(Asus.class);
            asus.since(2022);
            asus.name();

            Mac mac = (Mac) ComputerFactory.createComputer(Mac.class);
            mac.name();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}