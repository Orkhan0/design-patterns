package design_patterns.creational.factory.simple_example;

import java.util.logging.SocketHandler;

public class Asus implements Computer {


    @Override
    public void name() {
        System.out.println("This Computer's name is ASUS");
    }

    @Override
    public void since(int year) {
        System.out.println("Taken in " + year);
    }

}