package design_patterns.creational.factory.simple_example;

public class Mac implements Computer{
    @Override
    public void name() {
        System.out.println("Computer's mark is MAC");
    }

    @Override
    public void since(int year) {
        System.out.println("Taken in " + year);
    }


}